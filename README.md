# online-store-beka

#Lumen PHP Framework

**Persiapan**

- Install PHP 7.*
- Install Composer [](https://getcomposer.org/download/) 

**Proses Installasi**

- Clone the Repo
- `composer install`
- Ubah file .env.example menjadi .env
- isi .env: 
    - DB_CONNECTION=pgsql
    - DB_HOST=localhost
    - DB_PORT=5432
    - DB_DATABASE=db_online_store
    - DB_USERNAME=[user database]
    - DB_PASSWORD=[password database]

- `php artisan migrate`
- `php artisan db:seed`

**Menjalankan Service**
- `php -S localhost:8000 -t public`

**API Documentation**
- `http://localhost:8000/api/documentation`


# Deskripsi Soal Online Store
1.  Untuk kasus event 12.12 terjadi karena konsumen tidak mendapat informasi persediaan/stock paling baru saat pemesanan masuk ke keranjang (Cart) kemudian di proses (Checkout). Sehingga proses pemesanan tetap bisa di lakukan tanpa adanya pengecekan ke persediaan/stock.
2.  Solusi untuk masalah ini yaitu harus dilakukan pengecekan ke persediaan/stock di beberapa titik di system ketika pemesanan.
3.  Untuk setiap pemesanan yang akan masuk ke keranjang, system akan mengecek jumlah produk yang di pesan ke persediaan/stock. selain di keranjang pengecekan jumlah produk ke persediaan/stock juga di lakukan ketika proses Checkout dimana apabila produk masih tersedia di persediaan maka proses bisa dilanjutkan ke pembayaran dan mengurangi jumlah produk di persediaan. Namun jika jumlah produk tidak tersedia di persediaan system akan menghentikan proses pemesanan, sehingga tidak bisa melanjutkan ke tahap pembayaran. 


# Treasure Hunt
_url_**** `http://localhost:8000/treasure-hunt`

- Tekan tombol start untuk mengubah posisi [$]
- Arah kan [X] ke atas (Key A), kanan (Key B) & bawah (Key C) sampai ke posisi [$]
- Pastikan untuk Key menggunakan huruf capital (CAPS LOCK)


