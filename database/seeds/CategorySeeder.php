<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ref_product_category')->insert([
            'id' => 'a0df1c1a-238a-46e7-b2a4-af8dcce745da',
            'category' => 'Pakaian',
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
