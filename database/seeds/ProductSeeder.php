<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ref_product')->insert([
            'id' => '25c89f16-eeb6-4b93-8204-52e8fd9263fa',
            'id_category' => 'a0df1c1a-238a-46e7-b2a4-af8dcce745da',
            'name' => 'Kaos Polos',
            'description' => 'Kaos Polos',
            'price' => 50000,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('ref_product')->insert([
            'id' => '7fd9fd21-2a63-4eee-844b-689f44d434e0',
            'id_category' => 'a0df1c1a-238a-46e7-b2a4-af8dcce745da',
            'name' => 'Kemeja',
            'description' => 'Kemeja',
            'price' => 98000,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('ref_product')->insert([
            'id' => 'e884cd1b-b09a-4493-a5bd-33954216d99c',
            'id_category' => 'a0df1c1a-238a-46e7-b2a4-af8dcce745da',
            'name' => 'Kaos Lengan Panjang',
            'description' => 'Kaos Lengan Panjang',
            'price' => 75000,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
