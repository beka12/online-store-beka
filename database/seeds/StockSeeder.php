<?php

use Illuminate\Database\Seeder;

class StockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trans_stock')->insert([
            'id' => '55ec274b-0e86-4247-9dbb-6ae68fba394d',
            'id_product' => 'e884cd1b-b09a-4493-a5bd-33954216d99c',
            'qty' => 10,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('trans_stock')->insert([
            'id' => '984e1f57-6810-4441-8acc-7ad3d54fa118',
            'id_product' => '25c89f16-eeb6-4b93-8204-52e8fd9263fa',
            'qty' => 10,
            'status' => 5,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('trans_stock')->insert([
            'id' => '211a6ad2-3fe9-4cfe-9721-3431eca7fc44',
            'id_product' => '7fd9fd21-2a63-4eee-844b-689f44d434e0',
            'qty' => 3,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
