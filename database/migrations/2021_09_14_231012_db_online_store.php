<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DbOnlineStore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('id_user');
            $table->string('username');
            $table->string('email');
            $table->string('phone');
            $table->string('password');
            $table->string('fullname');
            $table->timestamp('register_date');
            $table->tinyInteger('status');
            $table->primary('id_user');
            $table->unique('email');
            $table->unique('phone');
        });

        Schema::create('ref_product', function (Blueprint $table) {
            $table->string('id');
            $table->string('id_category');
            $table->string('name');
            $table->text('description');
            $table->float('price');
            $table->tinyInteger('status');
            $table->timestamps();
            $table->primary('id');
            $table->unique('name');
        });

        Schema::create('ref_product_category', function (Blueprint $table) {
            $table->string('id');
            $table->string('category');
            $table->tinyInteger('status');
            $table->timestamps();
            $table->primary('id');
            $table->unique('category');
        });

        Schema::create('trans_cart', function (Blueprint $table) {
            $table->string('id');
            $table->string('id_user');
            $table->string('id_product');
            $table->float('qty');
            $table->float('price');
            $table->float('subtotal');
            $table->tinyInteger('status');
            $table->timestamps();
            $table->primary('id');
        });

        Schema::create('trans_order', function (Blueprint $table) {
            $table->string('id');
            $table->string('id_user');
            $table->timestamp('trans_date');
            $table->float('total_qty');
            $table->float('total_amount');
            $table->tinyInteger('status');
            $table->timestamps();
            $table->primary('id');
        });

        Schema::create('trans_order_item', function (Blueprint $table) {
            $table->string('id');
            $table->string('id_cart');
            $table->string('id_order');
            $table->string('id_product');
            $table->float('qty');
            $table->float('price');
            $table->float('subtotal');
            $table->tinyInteger('status');
            $table->timestamps();
            $table->primary('id');
        });

        Schema::create('trans_stock', function (Blueprint $table) {
            $table->string('id');
            $table->string('id_product');
            $table->float('qty');
            $table->tinyInteger('status');
            $table->timestamps();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
        Schema::drop('ref_product');
        Schema::drop('ref_product_category');
        Schema::drop('trans_cart');
        Schema::drop('trans_order');
        Schema::drop('trans_order_item');
        Schema::drop('trans_stock');
    }
}