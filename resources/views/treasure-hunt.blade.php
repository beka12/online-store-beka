<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="margin: 0;padding: 0;font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;line-height: 1.25;">
<head style="margin: 0;padding: 0;font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;line-height: 1.25;">
	<meta name="viewport" content="width=600" style="margin: 0;padding: 0;font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;line-height: 1.25;">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" style="margin: 0;padding: 0;font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;line-height: 1.25;">
	<title style="margin: 0;padding: 0;font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;line-height: 1.25;">TREASURE HUNT</title>
</head>

<body>
	<table id='treasure-map'>
        <tr>
            <td id='c11'>#</td>
            <td id='c21'>#</td>
            <td id='c31'>#</td>
            <td id='c41'>#</td>
            <td id='c51'>#</td>
            <td id='c61'>#</td>
            <td id='c71'>#</td>
            <td id='c81'>#</td>
        </tr>
        <tr>
            <td id='c12'>#</td>
            <td id='c22'>.</td>
            <td id='c32'>.</td>
            <td id='c42'>.</td>
            <td id='c52'>.</td>
            <td id='c62'>.</td>
            <td id='c72'>$</td>
            <td id='c82'>#</td>
        </tr>
        <tr>
            <td id='c13'>#</td>
            <td id='c23'>.</td>
            <td id='c33'>#</td>
            <td id='c43'>#</td>
            <td id='c53'>#</td>
            <td id='c63'>.</td>
            <td id='c73'>.</td>
            <td id='c83'>#</td>
        </tr>
        <tr>
            <td id='c14'>#</td>
            <td id='c24'>.</td>
            <td id='c34'>.</td>
            <td id='c44'>.</td>
            <td id='c54'>#</td>
            <td id='c64'>.</td>
            <td id='c74'>#</td>
            <td id='c84'>#</td>
        </tr>
        <tr>
            <td id='c15'>#</td>
            <td id='c25'>X</td>
            <td id='c35'>#</td>
            <td id='c45'>.</td>
            <td id='c55'>.</td>
            <td id='c65'>.</td>
            <td id='c75'>.</td>
            <td id='c85'>#</td>
        </tr>
        <tr>
            <td id='c16'>#</td>
            <td id='c26'>#</td>
            <td id='c36'>#</td>
            <td id='c46'>#</td>
            <td id='c56'>#</td>
            <td id='c66'>#</td>
            <td id='c76'>#</td>
            <td id='c86'>#</td>
        </tr>
    </table>

    <button id="btn-start">START</button>
</body>
<script src="{{ 'js/jquery.js' }}"></script>
<script>
    $(function() {
        // $table
        var start_x = 2;
        var start_y = 5;
        $(document).keypress(function(e) {
            if(e.which == 65) {
                $('#c'+start_x+start_y).text('.');
                start_y = start_y - 1; 
                switch($('#c'+start_x+start_y).text()){
                    case '#':
                        alert('Hindari #');
                        start_y = start_y - (-1);
                        $('#c'+start_x+start_y).text('X');
                        break;
                    case '$':
                        alert('Selamat Anda menemukan Harta karun nya');
                        $('#c'+start_x+start_y).text('OK');
                        break;
                    case '.':
                        $('#c'+start_x+start_y).text('X');
                        break;
                }
            }


            if(e.which == 66) {
                $('#c'+start_x+start_y).text('.');
                start_x = start_x - (-1); 
                switch($('#c'+start_x+start_y).text()){
                    case '#':
                        alert('Hindari #');
                        start_x = start_x -1;
                        $('#c'+start_x+start_y).text('X');
                        break;
                    case '$':
                        alert('Selamat Anda menemukan Harta karun nya');
                        $('#c'+start_x+start_y).text('OK');
                        break;
                    case '.':
                        $('#c'+start_x+start_y).text('X');
                        break;
                }
            }

            if(e.which == 67) {
                $('#c'+start_x+start_y).text('.');
                start_y = start_y - (-1);
                switch($('#c'+start_x+start_y).text()){
                    case '#':
                        alert('Hindari #');
                        start_y = start_y -1;
                        $('#c'+start_x+start_y).text('X');
                        break;
                    case '$':
                        alert('Selamat Anda menemukan Harta karun nya');
                        $('#c'+start_x+start_y).text('OK');
                        break;
                    case '.':
                        $('#c'+start_x+start_y).text('X');
                        break;
                }
            }
        });
        $('#btn-start').click(function(){
            var array = ['22','32','42','52','62','23','63','73','24','34','44','64','45','55','65','75'];
            var tableRow = $("td").filter(function() {
                return $(this).text() == "$";
            }).closest("tr");
            $(tableRow.prevObject[0]).text('.');
            $('#c'+array.random()).text('$');
        });

        Array.prototype.random = function () {
            return this[Math.floor((Math.random()*this.length))];
        }
    });
</script>
</html>