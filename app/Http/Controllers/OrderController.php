<?php
namespace App\Http\Controllers;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\CustomHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\Stock;
use App\Models\Cart;
use App\Models\User;

class  OrderController extends Controller
{
    private $request;
    private $helpers;

    public function __construct( Request $request,CustomHelper $helpers)
    {
        date_default_timezone_set('Asia/Jakarta');
        $this->middleware('auth');
        $this->request = $request;
        $this->helpers = $helpers;
    }

    /*
     * API Get data Order / List Product
     */
    /**
     * @OA\Get(
     *   path="/api/list-product",
     *   summary="Data Produk",
     *   tags={"Order"},
     *   security={{"api_key": {}}},
     *   @OA\Response(
     *     response="200",
     *     description="success"
     *   ),
     *   @OA\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    public function get_product()
    {
       try {
        $data = Product::with(['category'])
            ->where('status','=',1)
            ->get();
        if($data->count() == 0){
            $res['code'] = 200;
            $res['message'] = "Data empty.";
            return response()->json($res, 200);
        }
        $res['code'] = 200;
        $res['message'] = "Data Stored.";
        $res['data'] = $data;
        $res['id'] = $this->helpers->genId();
        return response()->json($res, 200); 
       } catch (\Exception $e) {
           $res['code'] = 500;
           $res['message'] = $e->getMessage();
           return response()->json($res, 500);
       }
    }

    /*
     * API Post Add to Cart
     */
    /**
     * @OA\Post(
     *   path="/api/cart",
     *   summary="Simpan Pesanan ke Keranjang",
     *   tags={"Order"},
     *   security={{"api_key": {}}},
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(
     *              @OA\Property(property="id_product", type="string"),
     *               @OA\Property(property="qty", type="string"),
     *               @OA\Property(property="price", type="string"),
     *               @OA\Property(property="subtotal", type="string")
     *          )
     *       )
     *    ),
     *   @OA\Response(
     *     response="200",
     *     description="success"
     *   ),
     *   @OA\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    public function manage_cart()
    {
        $validator = Validator::make($this->request->all(), [
            'id_product' => 'required|string',
            'qty' => 'required|string',
            'price' => 'required|string'
        ]);

        if ($validator->fails()) {
            $fields = '';
            foreach($validator->errors()->all() as $key => $value){
                $fields .= 'The '.$value.', ';
            }
            $res['code'] = 400;
            $res['error'] = $fields;
            return response()->json($res, 400);
        }

        try {
            DB::beginTransaction();
            if($this->check_stock($this->request->input('id_product'),$this->request->input('qty')) >= 0){
                $user = Auth::guard()->user();
                $check_cart = Cart::where('id_product','=',$this->request->input('id_product'))->first();
                if($check_cart){
                    $qty = $this->request->input('qty') + $check_cart->qty;
                    $subtotal = $qty *  $this->request->input('price');
                    $update = Cart::where('id_product','=',$this->request->input('id_product'))
                                ->where('id_user','=',$user->id_user)
                                ->update(['qty' => $qty, 'subtotal' => $subtotal ]);
                    if($update)
                    {
                        DB::commit();
                        $res['code'] = 201;
                        $res['message'] = 'Item successfully added to cart.';
                        return response()->json($res, 201);
                    }
                }else{
                    $data = new Cart;
                    $data->id = $this->helpers->genId();
                    $data->id_user = $user->id_user;
                    $data->id_product = $this->request->input('id_product');
                    $data->price = $this->request->input('price');
                    $data->qty = $this->request->input('qty');
                    $data->subtotal = intval($this->request->input('price')) * $data->qty;
                    $data->status = 1;
                    if($data->save())
                    {
                        DB::commit();
                        $res['code'] = 201;
                        $res['message'] = 'Item successfully added to cart.';
                        return response()->json($res, 201);
                    }
                }
                
                
            }else{
                $res['code'] = 201;
                $res['message'] = 'Product Out Of Stock.';
                return response()->json($res, 201);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            $res['code'] = 500;
            $res['message'] = $e->getMessage();
            return response()->json($res, 500);
        }
    }

    /*
     * API Delete Cart
     */
    /**
     * @OA\Post(
     *   path="/api/cart/delete",
     *   summary="Hapus produk dari keranjang",
     *   tags={"Order"},
     *   security={{"api_key": {}}},
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(
     *              @OA\Property(property="id", type="string")
     *          )
     *       )
     *    ),
     *   @OA\Response(
     *     response="200",
     *     description="success"
     *   ),
     *   @OA\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    public function delete_cart()
    {
        $validator = Validator::make($this->request->all(), [
            'id' => 'required|string'
        ]);

        if ($validator->fails()) {
            $fields = '';
            foreach($validator->errors()->all() as $key => $value){
                $fields .= 'The '.$value.', ';
            }
            $res['code'] = 400;
            $res['error'] = $fields;
            return response()->json($res, 400);
        }

        try {
            DB::beginTransaction();
            $data = Cart::where('id','=', $this->request->input('id'))->delete();
            if($data)
            {
                DB::commit();
                $res['code'] = 201;
                $res['message'] = 'Process Successfully.';
                return response()->json($res, 201);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            $res['code'] = 500;
            $res['message'] = $e->getMessage();
            return response()->json($res, 500);
        }
    }
    
    /*
     * API Get Cart
     */
    /**
     * @OA\Get(
     *   path="/api/cart",
     *   summary="Data Pesanan di Keranjang",
     *   description="Endpoint untuk menampilkan daftar belanjaan yang ada di keranjang. dimana terdapat attribute stock_status untuk menunjukan apakah Stock Tersedia atau tidak.",
     *   tags={"Order"},
     *   security={{"api_key": {}}},
     *   @OA\Response(
     *     response="200",
     *     description="success"
     *   ),
     *   @OA\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */

    public function get_cart()
    {
        try {
            $user = Auth::guard()->user();
            $data = Cart::with(['product'])
                    ->leftJoin('trans_stock','trans_stock.id_product','=','trans_cart.id_product')
                    ->where('trans_cart.status','=',1)
                    ->where('trans_cart.id_user','=',$user->id_user)
                    ->select(
                        'trans_cart.id',
                        'trans_cart.id_product',
                        'trans_cart.qty',
                        'trans_cart.price',
                        'trans_cart.subtotal',
                        'trans_stock.qty as stock'
                    )
                    ->get();
            if($data->count() == 0){
                $res['code'] = 400;
                $res['message'] = "Data empty.";
                return response()->json($res, 200);
            }

            foreach($data as $key => $value){
                if($value->stock > 0){
                    $value->stock_status = 'Available';
                }else{
                    $value->stock_status = 'Out Of Stock';
                }
            }

            $res['code'] = 200;
            $res['message'] = "Data Stored.";
            $res['data'] = $data;
            return response()->json($res, 200);
        } catch (\Exception $e) {
            $res['code'] = 500;
            $res['message'] = $e->getMessage();
            return response()->json($res, 500);
        }
    }

    /*
     * API Post Manage
     */
    /**
     * @OA\Post(
     *   path="/api/checkout",
     *   summary="Proses checkout",
     *   tags={"Order"},
     *   security={{"api_key": {}}},
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(
     *               @OA\Property(property="total_qty", type="string", example="10"),
     *               @OA\Property(property="total_amount", type="string", example="200000"),
     *               @OA\Property(
     *                  property="items",
     *                  type="array",
     *                  @OA\Items(
     *                       type="object",
     *                       @OA\Property(property="id_cart", type="string"),
     *                       @OA\Property(property="id_product", type="string"),
     *                       @OA\Property(property="qty", type="string"),
     *                       @OA\Property(property="price", type="string"),
     *                       @OA\Property(property="subtotal", type="string")
     *                  )
     *              )
     *          )
     *       )
     *    ),
     *   @OA\Response(
     *     response="200",
     *     description="success"
     *   ),
     *   @OA\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */

    public function manage()
    {
        $validator = Validator::make($this->request->all(), [
            'total_qty' => 'required',
            'total_amount' => 'required',
            'items' => 'required'
        ]);

        if ($validator->fails()) {
            $fields = '';
            foreach($validator->errors()->all() as $key => $value){
                $fields .= 'The '.$value.', ';
            }
            $res['code'] = 400;
            $res['error'] = $fields;
            return response()->json($res, 400);
        }

        try {
            DB::beginTransaction();
            $user = Auth::guard()->user();
            $data = new Order;
            $data->id = $this->helpers->genId();
            $data->id_user = $user->id_user;
            $data->trans_date = date('Y-m-d H:i:s');
            $data->total_qty = $this->request->input('total_qty');
            $data->total_amount = $this->request->input('total_amount');
            $data->status = 1;
            if($data->save())
            {
                foreach($this->request->input('items') as $key => $value)
                {
                    Cart::where('id','=',$value['id_cart'])->delete();
                    $items = array();
                    $items['id'] = $this->helpers->genId();
                    $items['id_cart'] = $value['id_cart'];
                    $items['id_order'] = $data->id;
                    $items['id_product'] = $value['id_product'];
                    $items['qty'] = $value['qty'];
                    $items['price'] = $value['price'];
                    $items['subtotal'] = $value['subtotal'];
                    $items['status'] = 1;
                    if($this->check_stock($value['id_product'],$value['qty']) >= 0){
                        OrderItem::insert($items);
                        $this->manage_stock($value['id_product'],$value['qty'] );
                    }else{
                        $res['code'] = 201;
                        $res['message'] = 'Product Out Of Stock.';
                        return response()->json($res, 201);
                    }
                }
                
                DB::commit();
                $res['code'] = 201;
                $res['message'] = 'Checkout Successfully.';
                return response()->json($res, 201);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            $res['code'] = 500;
            $res['message'] = $e->getMessage();
            return response()->json($res, 500);
        }
    }

    public function void()
    {
        $validator = Validator::make($this->request->all(), [
            'id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            $fields = '';
            foreach($validator->errors()->all() as $key => $value){
                $fields .= 'The '.$value.', ';
            }
            $res['code'] = 400;
            $res['error'] = $fields;
            return response()->json($res, 400);
        }

        try {
            $master = Broadcast::where('id','=',$this->request->input('id'))
                        ->delete();
            $res['code'] = 201;
            $res['message'] = 'Deleted Success Created.';
            return response()->json($res, 201);
        } catch (\Exception $e) {
            $res['code'] = 500;
            $res['message'] = $e->getMessage();
            return response()->json($res, 500);
        }

    }

    /*
     * API Get History Order
     */
    /**
     * @OA\Get(
     *   path="/api/history",
     *   summary="Get Order History",
     *   tags={"Order"},
     *   security={{"api_key": {}}},
     *   @OA\Response(
     *     response="200",
     *     description="success"
     *   ),
     *   @OA\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    public function history()
    {
        $data = Order::with(['items','member']);

        if($this->request->get('condition')){
            $data->where(function($query){
               $query->orWhere('trans_order.order_number','like', '%'.$this->request->get('condition').'%');
            });
        }
        
        if($this->request->get('start_date')){
             $data->whereDate('trans_order.trans_date','>=', $this->request->get('start_date'));
             $data->whereDate('trans_order.trans_date','<=', $this->request->get('end_date'));
        }
        //ORDER BY
        if($this->request->get('dir')){
            $data->orderBy($this->request->get('dir'), $this->request->get('sort'));
        }else{
            $data->orderBy('trans_order.trans_date', 'DESC');
        }
        //PAGINATION
        $page = $this->request->get('page') != '' ? (int)$this->request->get('page') : 1;
        $rows = $this->request->get('rows') != '' &&  $this->request->get('rows') != 0 ? (int)$this->request->get('rows') : 10;
        $dir = $this->request->get('dir');
        $sort = $this->request->get('sort');

        $total = $data->count();
        $start = ($page > 1) ? $page : 0;
        $start = ($total <= $rows) ? 0 : $start;
        $pages = ceil($total / $rows);
        $data->offset($start);
        $data->limit($rows);
        $grid = $data->get();
        foreach($grid as $key => $item){
            $item->total_amount = (!$item->total_amount)? 0 :$item->total_amount;
            switch($item->status_order){
                case 1:
                    $text_info =  "Belum Bayar";
                    switch($item->status_shipping){
                        case 1 : 
                            $item->status_Pembelian = $text_info ." & Barang Belum Dipacking.";
                            break;
                        case 2 : 
                            $item->status_Pembelian = $text_info ." & Barang Sudah Dipacking.";
                            break;
                        case 3 : 
                            $item->status_Pembelian = $text_info ." & Barang Sudah Dikirim.";
                            break;
                        case 4 : 
                            $item->status_Pembelian = $text_info ." & Barang Sudah Diterima.";
                            break;
                    }
                    break;
                case 2 :
                    $text_info =  "Sudah Bayar";
                    switch($item->status_shipping){
                        case 1 : 
                            $item->status_Pembelian = $text_info ." & Barang Belum Dipacking.";
                            break;
                        case 2 : 
                            $item->status_Pembelian = $text_info ." & Barang Sudah Dipacking.";
                            break;
                        case 3 : 
                            $item->status_Pembelian = $text_info ." & Barang Sudah Dikirim.";
                            break;
                        case 4 : 
                            $item->status_Pembelian = $text_info ." & Barang Sudah Diterima.";
                            break;
                    }
            }
        }
        
        $res['code'] = 200;
        $res['message'] = "Data Stored.";
        $res['totalRow'] = $total;
        $res['totalPages'] = $pages;
        $res['data'] = $grid;
        return response()->json($res, 200);
    }

    public function check_stock($id_product, $qty){
        $data = Stock::where('id_product','=', $id_product)
                ->first();
        return ($data->qty - $qty);
    }

    public function manage_stock($id_product, $qty){
        $get_stock = Stock::where('id_product','=', $id_product)->first();
        $stock = $get_stock->qty - $qty;
        $manage = Stock::where('id_product','=', $id_product)
                ->update(['qty'=>$stock]);
        return $manage;
    }

}
