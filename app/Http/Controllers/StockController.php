<?php
namespace App\Http\Controllers;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\CustomHelper;
use App\Models\Stock;

class  StockController extends Controller
{
    private $request;
    private $helpers;

    public function __construct( Request $request, CustomHelper $helpers)
    {
        date_default_timezone_set('Asia/Jakarta');
        $this->middleware('auth');
        $this->request = $request;
        $this->helpers = $helpers;
    }

    /**
     * @OA\Get(
     *   path="/api/stock",
     *   summary="Get Data Stock",
     *   tags={"Order"},
     *   security={{"api_key": {}}},
     *   @OA\Response(
     *     response="200",
     *     description="success"
     *   ),
     *   @OA\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    public function get_summary()
    {
        $data = Stock::with(['product'])
                ->get();
        $res['code'] = 200;
        $res['message'] = "Data Stored.";
        $res['data'] = $data;
        return response()->json($res, 200);
    }

}
