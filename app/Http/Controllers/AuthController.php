<?php
namespace App\Http\Controllers;

use Validator;
use App\Models\Order;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\CustomHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

use Tymon\JWTAuth\JWTAuth;

class  AuthController extends Controller
{
    protected $jwt;
    private $request;
    private $email;
    private $auth;
    private $helpers;
   
    public function __construct(JWTAuth $jwt, Request $request, Auth $auth, CustomHelper $helpers)
    {
        $this->jwt = $jwt;
        $this->request = $request;
        $this->auth = $auth;
        $this->helpers = $helpers;
    }

    /**
     * @OA\Post(
     *   path="/auth/register",
     *   summary="Registrasi User",
     *   tags={"Login System"},
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(
     *                  @OA\Property(property="username", type="string", example="beka12"),
     *                  @OA\Property(property="phone", type="string", example="08572204884p"),
     *                  @OA\Property(property="email", type="string", example="prastiyo.beka12@gmail.com"),
     *                  @OA\Property(property="password", type="string", example="123456"),
     *                  @OA\Property(property="fullname", type="string", example="Prastiyo Beka"),
     *          )
     *       )
     *    ),
     *   @OA\Response(
     *     response="201",
     *     description="data created."
     *   ),
     *  @OA\Response(
     *     response="400",
     *     description="field required."
     *   ),
     *   @OA\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    public function register()
    {
        //validate incoming request 
        $validator = Validator::make($this->request->all(), [
            'username' => 'required|string',
            'phone' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            $fields = '';
            foreach($validator->errors()->all() as $key => $value){
                $fields .= 'The '.$value.', ';
            }
            $res['code'] = 400;
            $res['message'] = $fields;
            return response()->json($res, 400);
        }

        try {
            DB::beginTransaction();
            $check_email = User::where('email','=',$this->request->input('email'))->first();
            if(!empty($check_email)){
                $res['code'] = 401;
                $res['message'] = 'email already in use!';
                return response()->json($res, 401);
            }
            
            $user = new User;
            $user->id_user = $this->helpers->genId();
            $user->username = $this->request->input('username');
            $user->email = $this->request->input('email');
            $user->phone = $this->request->input('phone');
            $user->fullname = $this->request->input('fullname');
            $user->register_date = date('Y-m-d H:i:s');
            $user->password = app('hash')->make($this->request->input('password'));
            $user->status = 1;
            if($user->save()){
                DB::commit();
                $res['code'] = 201;
                $res['message'] = 'Register success.';
                return response()->json($res, 201);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            $res['code'] = 500;
            $res['message'] = $e->getMessage();
            return response()->json($res, 500);
        }

    }

    /**
     * @OA\Post(
     *   path="/auth/login",
     *   summary="Login User",
     *   tags={"Login System"},
     *   @OA\RequestBody(
     *       required=true,
     *       @OA\MediaType(
     *          mediaType="application/json",
     *          @OA\Schema(
     *                  @OA\Property(property="email",example="prastiyo.beka12@gmail.com / beka12",type="string"),
     *                  @OA\Property(property="password",example="123456", type="string")
     *          )
     *       )
     *    ),
     *   @OA\Response(
     *     response="200",
     *     description="success"
     *   ),
     *   @OA\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    public function login()
    {   
        $validator = Validator::make($this->request->all(), [
            'email'    => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $fields = '';
            foreach($validator->errors()->all() as $key => $value){
                $fields .= 'The '.$value.', ';
            }
            $res['code'] = 400;
            $res['message'] = $fields;
            return response()->json($res, 400);
        }

        $user = User::where(function($q){
            $q->where('email','=',$this->request->input('email'));
            $q->orWhere('username','=', strtolower($this->request->input('email')));
        })->first();
        if (empty($user)) {
            $res['code'] = 404;
            $res['message'] = 'User not found';
            return response()->json($res, 404);
        }
        try {
            if (!$token = $this->jwt->attempt(array('email' => $user->email, 'password' => $this->request->input('password')))) {
                $res['code'] = 401;
                $res['message'] = 'You email or password incorrect!';
                return response()->json($res, 401);
            }
           
            if ($user->status != 1) {
                $res['code'] = 401;
                $res['message'] = 'User not active';
                return response()->json($res, 401);
            }

            $data = [
                'uid' => $user->id_user,
                'username' => $user->username,
                'fullname' => $user->fullname,
                'email' => $user->email,
                'phone' => $user->phone
            ];
           
            $res['code'] = 201;
            $res['data'] = $data;
            $res['token'] = 'Bearer '.$token;
            return response()->json($res, 201);
        } catch (TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (JWTException $e) {
            return response()->json(['token_absent' => $e->getMessage()], $e->getStatusCode());
        }
    }

}
