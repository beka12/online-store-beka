<?php
namespace App\Http\Controllers;

use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Role;
use App\Helpers\CustomHelper;

class  UserController extends Controller
{
    private $request;
    private $helpers;

    public function __construct( Request $request, CustomHelper $helpers)
    {
        $this->middleware('auth');
        $this->request = $request;
        $this->helpers = $helpers;
    }

    /**
     * @OA\Post(
     *   path="/api/admin/logout",
     *   summary="Logout Admin",
     *   tags={"Login System"},
     *   security={{"api_key": {}}},
     *   @OA\Response(
     *     response="200",
     *     description="success"
     *   ),
     *   @OA\Response(
     *     response="500",
     *     description="error"
     *   )
     * )
     */
    public function logout()
    {
        $data = Auth::logout();
        $res['code'] = 200;
        $res['message'] = "Successfully logged out.";
        return response()->json($res, 200);
    }
}
