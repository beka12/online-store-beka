<?php

use \Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

/**
 * @OA\Info(
 *   version="1.0.0",
 *   title="Online Store Api Documentation",
 *   @OA\Contact(
 *     email="prastiyo.beka12@gmail.com"
 *   )
 * )
 * @OA\Schemes(format="http")
 * @OA\SecurityScheme(
 *      securityScheme="api_key",
 *      type="apiKey",
 *      in="header",
 *      name="Authorization"
 * ),
 */

$router->group(['prefix' => 'auth'], function ($router) {
    $router->post('register', 'AuthController@register');
    $router->post('login', 'AuthController@login');
});

$router->group(['prefix' => 'api', 'middleware'=>['cors']], function ($router) {
    $router->get('list-product', 'OrderController@get_product');
    $router->get('cart', 'OrderController@get_cart');
    $router->post('cart', 'OrderController@manage_cart');
    $router->post('cart/delete', 'OrderController@delete_cart');
    $router->get('history', 'OrderController@history');
    $router->post('checkout', 'OrderController@manage');
    $router->get('stock', 'StockController@get_summary');
});

$router->get('/treasure-hunt', function () use ($router) {
    return view('treasure-hunt');
});


$router->get('/', function () use ($router) {
    return $router->app->version();
});
