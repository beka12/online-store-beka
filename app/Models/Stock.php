<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model 
{
    protected $table = 'trans_stock';
    public $timestamps = true;
    public $incrementing = false;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $fillable = [
        'id',
        'id_product',
        'qty',
        'status'
    ];

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id','id_product');
    }

}
