<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model 
{
    protected $table = 'users_role';
    public $timestamps = true;
    public $incrementing = false;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $fillable = [
        'id_role',
        'role_name',
        'status'
    ];

}
