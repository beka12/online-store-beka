<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model 
{
    use \Awobaz\Compoships\Compoships;
    protected $table = 'ref_product_category';
    public $timestamps = true;
    public $incrementing = false;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $fillable = [
        'id',
        'category',
        'status'
    ];
}