<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model 
{
    protected $table = 'trans_order';
    public $timestamps = true;
    public $incrementing = false;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $fillable = [
        'id',
        'id_user',
        'trans_date',
        'total_qty',
        'total_amount',
        'status'
    ];

    public function items()
    {
    	return $this->hasMany('App\Models\OrderItem', 'id_order', 'id');
    }

    public function member()
    {
    	return $this->hasOne('App\User', 'id_user', 'id_user')->select('id_user','fullname');
    }
}
