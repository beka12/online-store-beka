<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model 
{
    use \Awobaz\Compoships\Compoships;
    protected $table = 'ref_product';
    public $timestamps = true;
    public $incrementing = false;
    const CREATED_AT = 'createdate';
    const UPDATED_AT = 'editedate';
    protected $fillable = [
        'id',
        'id_category',
        'name',
        'description',
        'price',
        'status'
    ];

    public function category()
    {
        return $this->hasOne('App\Models\Category', 'id','id_category');
    }

}