<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'trans_order_item';
    public $timestamps = false;
    public $incrementing = false;
    protected $fillable = [
        'id',
        'id_order',
        'id_product',
        'qty',
        'price',
        'subtotal',
        'status'
    ];

    public function order()
    {
    	return $this->belongsTo('App\Models\Order', 'id_order', 'id');
    }
}
