<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cart extends Model 
{
    use \Awobaz\Compoships\Compoships;
    protected $table = 'trans_cart';
    public $timestamps = true;
    public $incrementing = false;
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $fillable = [
        'id',
        'id_user',
        'id_product',
        'qty',
        'price',
        'subtotal',
        'created_at',
        'updated_at',
        'status'
    ];

    public function product()
    {
        return $this->hasOne('App\Models\Product', 'id','id_product')
        ->leftJoin('ref_product_category','ref_product_category.id','ref_product.id_category')
        ->select('ref_product.*','ref_product_category.category');
    }

    public function stock()
    {
    	return $this->hasMany('App\Models\Stock', 'id_product', 'id_product')
                ->select('id_product','qty');
    }
}
